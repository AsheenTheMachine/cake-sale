# README #

Cake sale console applcation

This branch allows the user to exit the application or continue, by entering 'Yes' or 'Exit'.

### Features ###

* C# Console Application
* .Net 5.0
* Recursion
* Mock database
* Table style menu
* Take customers order
* Calculate Total
* Accept payment
* Reduce stock count

### How do I get set up? ###

* Clone the repo to your local machine
* Open a new terminal
* navigate to your application folder
* type `dotnet run`


### Who do I talk to? ###

* email asheenk@gmail.com