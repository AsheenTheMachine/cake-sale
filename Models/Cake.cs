public class Cake
{
    public string name { get; set; }
    public decimal price { get; set; }
    public int quantity { get; set; }
}

