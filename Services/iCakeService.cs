using System.Collections.Generic;

public interface iCakeService
{
    IEnumerable<Cake> GetAll();
}