using System.Collections.Generic;

public class CakeCService : iCakeService
{
    private readonly List<Cake> cakes;

    public CakeCService()
    {
        cakes = new List<Cake>()
        {
            new Cake()
            {
                name = "Brownie ",
                price = 0.65m,
                quantity = 8
            },
            new Cake()
            {
                name = "Muffin",
                price = 1,
                quantity = 6
            },
            new Cake()
            {
                name = "Cake Pop",
                price = 1.35m,
                quantity = 3
            },
            new Cake()
            {
                name = "Water",
                price = 1.50m,
                quantity = 2
            }
        };
    }

    public IEnumerable<Cake> GetAll()
    {
        return cakes;
    }

}