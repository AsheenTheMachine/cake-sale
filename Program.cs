﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace cake_sale
{
    class Program
    {
        //database list
        private static List<Cake> cakesDB;

        private static string order = "";

        //order input
        private static string[] orderItems;

        //cake service
        private static CakeCService cakeService = new CakeCService();

        private static int tableWidth = 100;

        private static string status = "";

        static void Main(string[] args)
        {
            //load menu from database
            cakesDB = cakeService.GetAll().ToList();
            
            while (order != "exit")
            {
                //print menu to ui
                PrintMenu();

                //take the customers order
                decimal total = TakeOrder();

                //process the order if there are items in stock and a total amount is calculated > 0
                if (total > 0)
                {
                    Console.WriteLine($"Your order total is {string.Format("{0:C}", total)}");
                    Console.WriteLine();
                    Console.Write("Please enter your amount paying: ");

                    decimal payment = ReadPayment(total);

                    //correct amount received. then proceed to calculate change
                    decimal change = payment - total;

                    Console.WriteLine();
                    Console.WriteLine($"Your change is {change}");
                    Console.WriteLine("Have a nice day :)");
                    Console.WriteLine();
                    Console.WriteLine();

                    //remove cakes from stock
                    ReduceStockCount();

                    //check if the customer would like to place another order?
                    while (status.ToLower() != "yes" && status.ToLower() != "exit")
                    {
                        //ask if they would like to place another order, or exit
                        Console.Write("To place another order type 'Yes', to end the application, type 'exit' : ");
                        status = Console.ReadLine();

                        if (status == "exit")
                            Environment.Exit(0);
                    }
                }

                status = "";
            }
        }

        //method to take the customers order and return the total value amount
        private static decimal TakeOrder()
        {
            //if order = exit. end application
            if (order == "exit")
                Environment.Exit(0);

            while (string.IsNullOrEmpty(order))
            {
                Console.Write("Please enter your order: ");
                order = Console.ReadLine();
            }

            orderItems = order.Split(",".ToCharArray());

            decimal total = 0;
            bool outOfStock = false;
            //loop through cakes db and add price to total
            foreach (var item in orderItems)
            {
                Cake cake = cakesDB.Where(c => c.name.ToLower().StartsWith(item.ToLower())).FirstOrDefault();
                if (cake.quantity > 0)
                    total += cake.price;
                else
                {
                    outOfStock = true;
                    Console.Write($"We are out of stock for {cake.name}");
                    break;
                }
            }

            if (outOfStock)
            {
                Console.Write($"Please re-enter your order: ");
                order = Console.ReadLine();
                return TakeOrder();
            }

            return total;
        }

        //take the customers payment and check if payment is sufficient to complete the transaction
        private static decimal ReadPayment(decimal total)
        {
            string acceptedPayment = "";

            //amount customer paid
            acceptedPayment = Console.ReadLine();
            while (string.IsNullOrEmpty(acceptedPayment))
            {
                Console.Write("Please enter your payment amount to cover your total amount owing: ");
                acceptedPayment = Console.ReadLine();
            }

            //check if payment is a valid decimal value
            decimal payment = 0;
            bool paymentIsValid = decimal.TryParse(acceptedPayment, out payment);

            if (paymentIsValid)
            {
                //check if payment amount is less than total owing
                while (Decimal.Compare(payment, total) == -1)
                {
                    Console.Write("The amount you have entered is less than the total owing, please re-enter your payment amount: ");
                    payment = ReadPayment(total);
                }
            }

            return payment;
        }


        //subtract the ordered items from the stock totals
        private static void ReduceStockCount()
        {
            foreach (var cake in cakesDB)
            {
                foreach (var orderItem in orderItems)
                {
                    if (cake.name.ToLower().StartsWith(orderItem.ToLower()))
                    {
                        cake.quantity--;
                    }
                }
            }
        }


        #region Print Functions
        private static void PrintMenu()
        {
            Console.Clear();

            PrintLine();
            PrintRow("Choice", "Menu Item", "Price", "In-Stock");
            PrintLine();

            //print each row
            foreach (var item in cakesDB)
            {
                PrintRow(item.name.ToCharArray()[0].ToString(),
                         item.name,
                         string.Format("{0:C}", item.price).Trim(),
                         item.quantity.ToString());
            }

            PrintLine();
            Console.WriteLine();
            Console.Write("Good day :), Please enter your choice. You may enter multiple choices seperated by \",\". eg: B,C : ");

            order = Console.ReadLine();
        }

        private static void PrintLine()
        {
            Console.WriteLine(new string('-', tableWidth));
        }

        private static void PrintRow(params string[] columns)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
                row += AlignCentre(column, width) + "|";

            Console.WriteLine(row);
        }

        private static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
                return new string(' ', width);
            else
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
        }

        #endregion
    }
}

